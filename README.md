# Bowlls - Pegue as Bolas #

### Jogo desenvolvido para projeto integrador da faculdade SENAI/CIMATEC ###

Bowlls é um jogo simples, com poucas telas e que vai entreter seu usuário com o estilo infinito de jogo.

O jogador deve coletar todas as bolinhas coloridas com os blocos corretos para cada cor.

O jogo contará com duas cores, e à medida que o usuário vai acumulando pontos, a velocidade das bolas vai aumentando.

![device-2015-06-15-013004.png](https://bitbucket.org/repo/nrjkgj/images/563870536-device-2015-06-15-013004.png)
![device-2015-06-15-013111.png](https://bitbucket.org/repo/nrjkgj/images/3165463772-device-2015-06-15-013111.png)
![device-2015-06-15-013140.png](https://bitbucket.org/repo/nrjkgj/images/996938946-device-2015-06-15-013140.png)