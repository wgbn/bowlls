var Bloco = (function () {
    function Bloco(_cor, _posicao, _top, _game) {
        this.cor = _cor || 'red';
        this.posicao = _posicao || 0;
        this.umBloco = document.createElement('div');
        this.umBloco.setAttribute('class', 'bloco');
        this.umBloco.style.backgroundColor = this.cor;
        this.umBloco.style.left = this.posicao + 'px';
        this.umBloco.style.top = _top + 'px';
        _game.appendChild(this.umBloco);
    }
    Bloco.prototype.move = function (_posicao) {
        this.posicao += _posicao;
        this.umBloco.style.left = this.posicao + 'px';
    };
    Bloco.prototype.getPosicao = function () {
        return this.posicao;
    };
    Bloco.prototype.setCor = function (_cor, _rot) {
        this.cor = _cor;
        this.umBloco.style.backgroundColor = this.cor;
        this.umBloco.style.transform = 'rotate('+_rot+'deg)';
    };
    return Bloco;
})();