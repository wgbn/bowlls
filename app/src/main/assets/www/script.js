;(function(window, document){
    
    var App = (function(){
        
        var _private    = {
            htmlGame    : document.querySelector('.game'),
            htmlPonto   : document.querySelector('.pontos'),
            bolas       : [],
            cor         : 'blue',
            out         : 0,
            pontos      : 0
        };
        var _public     = {};
        
        _private.timerLancar;
        _private.alturaGame         = _private.htmlGame.offsetHeight || 0;
        _private.larguraGame        = _private.htmlGame.offsetWidth || 0;
        _private.alturaMetadeGame   = _private.alturaGame / 2;
        _private.alturaBlocos       = _private.alturaGame - 70;
        _private.blocoAzul          = new Bloco('blue', (_private.larguraGame - 60) / 2, _private.alturaBlocos, _private.htmlGame);
        _private.blocoVermelho      = new Bloco('red', (_private.larguraGame - 60) / 2, _private.alturaBlocos+20, _private.htmlGame);
        
        _public.init = function(){
            
            _private.htmlGame.addEventListener('click', _private.trocaBloco, false);
            
            _private.timerLancar = setInterval(_private.lancar, 800);
        };
        
        _private.lancar = function(){
            
            _private.bolas.push(new Bola(((new Date().getTime()) % 2) ? 'blue':'red', 0, new Date().getTime(), _private.htmlGame));
            _private.bolas[_private.bolas.length - 1].lancar();

            if (_private.pontos >= 10 && _private.pontos < 20){
                clearInterval(_private.timerLancar);
                _private.timerLancar = setInterval(_private.lancar, 700);
            } else {
                if (_private.pontos >= 20 && _private.pontos < 30){
                    clearInterval(_private.timerLancar);
                    _private.timerLancar = setInterval(_private.lancar, 600);
                } else {
                    if (_private.pontos >= 30 && _private.pontos < 40){
                        clearInterval(_private.timerLancar);
                        _private.timerLancar = setInterval(_private.lancar, 500);
                    } else {
                        if (_private.pontos >= 40 && _private.pontos < 50){
                            clearInterval(_private.timerLancar);
                            _private.timerLancar = setInterval(_private.lancar, 400);
                        } else {
                            if (_private.pontos >= 50){
                                clearInterval(_private.timerLancar);
                                _private.timerLancar = setInterval(_private.lancar, 300);
                            }
                        }
                    }
                }
            }
            
            if (_private.bolas.length > 10){
                _private.bolas.shift();
            }
            
        };
        
        _private.trocaBloco = function(e){
            
            if (_private.cor == 'blue')
                _private.cor = 'red';
            else
                _private.cor = 'blue';

            _private.blocoAzul.setCor(_private.cor == 'blue' ? 'blue':'red', _private.cor == 'blue' ? 180 : -180);
            _private.blocoVermelho.setCor(_private.cor == 'blue' ? 'red':'blue', _private.cor == 'blue' ? 180 : -180);
            
        };
        
        _public.marcaPonto = function(){
            
            _private.pontos++;
            _private.htmlPonto.innerHTML = _private.pontos;
            
        };
        
        _public.gameOver = function(){
            clearInterval(_private.timerLancar);
            
            if (typeof Android !== 'undefined'){
                if (!_private.out){
                    Android.salvaScore(_private.pontos);
                    Android.gameOver();
                }

                _private.out++;
            } else {
                _private.htmlGame.style.display = 'none';
            }
            
        };
        
        _public.getPontos   = function(){ return _private.pontos; };
        _public.getCor      = function(){ return _private.cor; };
        
        return _public;
        
    })();
    
    window.App = App;
    
})(window, document);

document.addEventListener("DOMContentLoaded", function(e){
    window.App.init();
});
