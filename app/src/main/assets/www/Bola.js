var Bola = (function () {
    function Bola(_cor, _posicao, _id, _game) {
        this.cor = _cor || 'red';
        this.posicao = _posicao || 0;
        this.game = _game;
        this.id = _id;

        this.umaBola = document.createElement('div');
        this.umaBola.setAttribute('class', 'bola');
        this.umaBola.setAttribute('id', 'bola-'+this.id);
        this.umaBola.style.backgroundColor = this.cor;
        this.umaBola.style.top = this.posicao + 'px';
        this.game.appendChild(this.umaBola);
    }
    Bola.prototype.move = function (_step) {
        _step = _step || 5;
        this.posicao += _step;
        this.umaBola.style.top = this.posicao + 'px';
    };
    Bola.prototype.lancar = function () {
        var timer = setInterval(mover, 50);
        var self = this;

        function mover(){
            if (self.getPosicao() < (self.game.offsetHeight - 90))
                if (App.getPontos() >= 10 && App.getPontos() < 30)
                    self.move(11);
                else
                    if (App.getPontos() >= 30 && App.getPontos() < 50)
                        self.move(12);
                    else
                        if (App.getPontos() >= 50 && App.getPontos() < 80)
                            self.move(13);
                        else
                            if (App.getPontos() >= 80 && App.getPontos() < 110)
                                self.move(14);
                            else
                                if (App.getPontos() >= 110)
                                    self.move(15);
                                else
                                    self.move(10);
            else 
                sair();
        }

        function sair(){
            clearInterval(timer);

            if (App.getCor() == self.cor)
                App.marcaPonto();
            else
                App.gameOver();

            self.destroi();
        }
    };
    Bola.prototype.destroi = function () {
        document.getElementById('bola-'+this.id).remove();

    };
    Bola.prototype.getPosicao = function () {
        return this.posicao;
    };
    return Bola;
})();
