package br.com.wgbn.bowlls;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import br.com.wgbn.bowlls.model.Score;


public class GameOver extends Activity {

    private TextView    txtSeuScore;
    private ImageButton btnJogarNovamente;
    private Score       score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        txtSeuScore = (TextView) findViewById(R.id.txtSeuScore);
        btnJogarNovamente = (ImageButton) findViewById(R.id.btnJogarNovamente);

        score = Score.getUltimoScore();
        txtSeuScore.setText(score.getPontuacaoString());

        btnJogarNovamente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Jogo.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        finish();
    }

    @Override
    public void onStop(){
        super.onStop();
        finish();
    }

}
