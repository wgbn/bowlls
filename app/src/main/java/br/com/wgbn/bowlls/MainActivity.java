package br.com.wgbn.bowlls;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import br.com.wgbn.bowlls.model.Score;


public class MainActivity extends Activity {

    private ImageButton btnJogar;
    private TextView    txtScore;
    private Score       score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnJogar = (ImageButton) findViewById(R.id.btnJogar);
        txtScore = (TextView) findViewById(R.id.txtScore);

        btnJogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Jogo.class);
                startActivity(intent);
            }
        });

        Score.setPrimeiroRegistro();

        score = Score.getMelhorScore();

        txtScore.setText(score.getPontuacaoString());
    }

    @Override
    public void onPause(){
        super.onPause();
        finish();
    }

    @Override
    public void onStop(){
        super.onStop();
        finish();
    }
}
