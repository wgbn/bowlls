package br.com.wgbn.bowlls;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.util.Date;

import br.com.wgbn.bowlls.model.Score;

public class JavaScript {
    private Context contexto;

    /** Instantiate the interface and set the context */
    public JavaScript(Context c) {
        contexto = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(contexto, toast, Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void gameOver() {
        Intent intent = new Intent(this.contexto.getApplicationContext(), GameOver.class);
        this.contexto.startActivity(intent);
    }

    @JavascriptInterface
    public void salvaScore(int _score){
        Date agora  = new Date();
        Score score = new Score();

        score.registro  = agora.getTime();
        score.pontuacao = _score;
        score.save();
    }
}
