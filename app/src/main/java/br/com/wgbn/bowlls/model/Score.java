package br.com.wgbn.bowlls.model;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Scores")
public class Score extends Model {
    @Column(name = "Registro")
    public long registro;

    @Column(name = "Pontuacao")
    public int pontuacao;

    public Score(){
        super();
    }

    public long getRegistro() {
        return this.registro;
    }

    public int getPontuacao() {
        return this.pontuacao;
    }

    public String getPontuacaoString() {
        return String.valueOf(this.pontuacao);
    }

    public Score(long _registro, int _pontuacao){
        super();
        this.registro = _registro;
        this.pontuacao = _pontuacao;
    }

    public static Score getMelhorScore(){
        return new Select()
                .from(Score.class)
                .orderBy("Pontuacao DESC")
                .executeSingle();
    }

    public static Score getUltimoScore(){
        return new Select()
                .from(Score.class)
                .orderBy("Registro DESC")
                .executeSingle();
    }

    public static void setPrimeiroRegistro(){
        List<Model> sc = new Select()
                .from(Score.class)
                .orderBy("Registro ASC")
                .execute();

        if (sc.size() <= 0){
            Score score = new Score(1,0);
            score.save();
        }
    }
}
