package br.com.wgbn.bowlls;

import android.app.Activity;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class Jogo extends Activity {

    private WebView webJogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogo);

        webJogo = (WebView) findViewById(R.id.webJogo);
        WebSettings s = webJogo.getSettings();
        s.setJavaScriptEnabled(true);
        s.setAllowFileAccess(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            s.setAllowUniversalAccessFromFileURLs(true);
            s.setAllowFileAccessFromFileURLs(true);
        }

        webJogo.addJavascriptInterface(new JavaScript(this), "Android");
        webJogo.setWebChromeClient(new WebChromeClient());
        webJogo.loadUrl("file:///android_asset/www/index.html");
    }

    @Override
    public void onPause(){
        super.onPause();
        finish();
    }

    @Override
    public void onStop(){
        super.onStop();
        finish();
    }

}
